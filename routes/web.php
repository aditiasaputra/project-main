<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\RequestWithdrawController;
use App\Http\Controllers\UsersController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->action([HomeController::class, 'index']);
});

Auth::routes([
    'register' => false,
    'reset' => false,
    'verify' => false,
]);

Route::get('/home', [HomeController::class, 'index'])->name('home');
Route::get('/user', [UsersController::class, 'index'])->name('user.index');

Route::resource('users', UsersController::class);
Route::get('/membership',[UsersController::class, 'membership'])->name('membership');
Route::get('/status-wd',[RequestWithdrawController::class, 'index'])->name('statuswd');
Route::post('/status-wd',[RequestWithdrawController::class, 'store'])->name('statuswd.store');
