<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Administrator',
                'email' => 'admin@admin.com',
                'password' => bcrypt('admin123'),
                'role_id' => 1
            ],
            [
                'name' => 'Budi',
                'email' => 'budi@gmail.com',
                'password' => bcrypt('budi123'),
                'role_id' => 2
            ],
            [
                'name' => 'Rizki',
                'email' => 'rizki@gmail.com',
                'password' => bcrypt('rizki123'),
                'role_id' => 2
            ],
        ];

        foreach ($users as $user) {
            User::create([
                'name' => $user['name'],
                'email' => $user['email'],
                'password' => $user['password'],
                'role_id' => $user['role_id'],
            ]);
        }
    }
}
