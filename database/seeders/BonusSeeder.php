<?php

namespace Database\Seeders;

use App\Models\Bonus;
use Illuminate\Database\Seeder;

class BonusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $bonus = [
            [
                'name' => 'Register',
                'bonus_value' => 10,
            ]
        ];

        foreach ($bonus as $bns) {
            Bonus::create([
                'name' => $bns['name'],
                'bonus_value' => $bns['bonus_value'],
            ]);
        }
    }
}
