<?php

namespace App\Http\Controllers;

use DataTables;
use App\Models\Log;
use App\Models\User;
use App\Models\Bonus;
use App\Models\Membership;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;


class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    } 

    public function index(Request $request)
    {
        $data = [
            'count_user' => User::latest()->count(),
            'menu'       => 'menu.v_menu_admin',
            'content'    => 'content.view_user',
            'title'    => 'Table User'
        ];

        if ($request->ajax()) {
            $q_user = User::select('*')->where('role_id','!=', 0)->orderByDesc('created_at');
            return Datatables::of($q_user)
                    ->addIndexColumn()
                    ->addColumn('action', function($row){
     
                        $btn = '<div data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Edit" class="btn btn-sm btn-icon btn-outline-success btn-circle mr-2 edit editUser"><i class=" fi-rr-edit"></i></div>';
                        $btn = $btn.' <div data-toggle="tooltip"  data-id="'.$row->id.'" data-original-title="Delete" class="btn btn-sm btn-icon btn-outline-danger btn-circle mr-2 deleteUser"><i class="fi-rr-trash"></i></div>';
 
                         return $btn;
                    })
                    ->rawColumns(['action'])
                    ->make(true);
        }

        return view('layouts.v_template',$data);
    }

    public function create()
    {
        //
    }

    public function store(Request $request)
    {
        $userData = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'role_id' => (int) $request->role_id,
            'reference_id' => (int)  $request->reference_id,
            'password' => Hash::make($request->password),
        ]);

        $userDataID = $userData->id;

        Membership::create([
            'reference_user_id' => $request->reference_id,
            'target_user_id' => $userDataID,
            'bonus_id' => (int) $request->bonus_id,
        ]);

        $totalBonus = User::find($request->reference_id)->total_bonus;

        $bonusValue = Bonus::find($request->bonus_id)->bonus_value;
        $jumlahBonus = $bonusValue + $totalBonus;
        
        User::where(['id' => $request->reference_id])->update(['total_bonus' => $jumlahBonus]);

        Log::create([
            'activity' => 'Register',
            'note' => 'Register New Member',
            'reference_id' => $userDataID,
            'log_user_id' => auth()->user()->id,
        ]);

        dump($bonusValue);
        dd('Ok');

        return response()->json(['success'=>'User saved successfully!']);
    }

    public function show($id)
    {
        //
    }

    public function edit($id)
    {
        $User = User::find($id);
        return response()->json($User);

    }

    public function update(Request $request, $id)
    {
        //
    }

    public function destroy($id)
    {
        User::find($id)->delete();

        return response()->json(['success'=>'Customer deleted!']);
    }

    public function membership()
    {
        $data = [
            'count_user' => User::latest()->count(),
            'menu'       => 'menu.v_menu_admin',
            'content'    => 'content.view_membership',
            'title'    => 'Form Add Membership',
            'users' => User::all()
        ];
        return view('layouts.v_template',$data);
    }
}
