<?php

namespace App\Http\Controllers;

use App\Models\User;
use App\Models\Withdraw;
use Illuminate\Http\Request;

class RequestWithdrawController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = [
            'count_user' => User::latest()->count(),
            'menu'       => 'menu.v_menu_admin',
            'content'    => 'content.view_withdraw',
            'title'    => 'Status Withdraw',
            'total_bonus'    => User::find(auth()->user()->id)->total_bonus,
            'withdraws' => Withdraw::where('wd_user_id', auth()->user()->id)->get(),
        ];
        return view('layouts.v_template', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $authUserId = auth()->user()->id;
        $totalWd = (int) $request->total_wd;
        $status = $request->status;

        Withdraw::create([
            'wd_user_id' => $authUserId,
            'accept_user_id' => null,
            'total_wd' => $totalWd,
            'status' => $status,
        ]);

        $totalBonus = User::find($authUserId)->total_bonus;
        $sisaBonus = $totalBonus - $totalWd;

        User::where(['id' => $authUserId])->update(['total_bonus' => $sisaBonus]);

        return redirect('status-wd')->with('success', 'Request withdraw sukses!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
