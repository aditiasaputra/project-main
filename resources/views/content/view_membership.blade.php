<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2>{{$title}}</h2>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                  <form id="formUser" method="POST" action="{{ route('users.store') }}">
                    @csrf
                    <input type="hidden" name="bonus_id" id="bonus_id" value="1">
                    <div class="form-group">
                      <label for="name">Nama
                        <span class="text-danger">*</span>
                      </label>
                      <input type="text" name="name" class="form-control" id="name" placeholder="Masukkan Nama" autofocus><br>
                    </div>
                    <div class="form-group">
                      <label for="name">Email 
                        <span class="text-danger">*</span>
                      </label>
                      <input type="email" name="email" class="form-control" id="email" placeholder="Masukkan Email"><br>
                    </div>
                    @if(auth()->user()->role_id == 1)
                      <div class="form-group">
                      <label for="name">Role
                        <span class="text-danger">*</span>
                      </label>
                      <select name="role_id" class="form-control" id="role_id">
                            <option value="-">Pilih Role</option>
                            <option value="1">Admin</option>
                            <option value="2" selected>Member</option>
                        </select><br>
                    </div>
                    @else
                      <input type="hidden" name="role_id" value="2">
                    @endif
                    @if(auth()->user()->role_id == 1)
                      <div class="form-group">
                        <label for="name">Referensi User
                          <span class="text-danger">*</span>
                        </label>
                        <select name="reference_id" class="form-control" id="reference_id">
                              <option value="-">Pilih Referensi User</option>
                              @foreach ($users as $user)
                              <option value="{{ $user->id }}">{{ $user->name }}</option>
                              @endforeach
                          </select><br>
                      </div>
                    @else
                      <input type="hidden" name="reference_id" value="{{ auth()->user()->id }}">
                    @endif
                    
                    <div class="form-group">
                      <label for="name">Password
                        <span class="text-danger">*</span>
                      </label>
                      <input type="text" name="password" class="form-control" placeholder="Masukkan Password"><br>
                    </div>
                    <button type="submit" class="btn btn-primary btn-block">Submit</button>
                  </form>
                </div>
            </div>
        </div>
    </div>
</div>