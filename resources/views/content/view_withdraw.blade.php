@if ($message = Session::get('success'))
  <div class="alert alert-info alert-block">
    <button type="button" class="close" data-dismiss="alert">×</button>    
    <strong>{{ $message }}</strong>
  </div>
@endif
<div class="content d-flex flex-column flex-column-fluid" id="kt_content">
    <div class="container">
        <div class="card">
            <div class="card-header">
                <h2>{{$title}}</h2>
                <div class="d-flex flex-row-reverse">
                  <button class="btn btn-sm btn-pill btn-outline-primary font-weight-bolder" id="requestWithdraw"><i class="fas fa-plus"></i>
                    Request Withdraw
                  </button>
                </div>
            </div>
            <div class="card-body">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table" id="tableUser">
                            <thead class="font-weight-bold text-center">
                                <tr>
                                    <th>No.</th>
                                    <th>Total WD</th>
                                    <th>Status</th>
                                </tr>
                            </thead>
                            <tbody class="text-center">
                                @forelse ($withdraws as $wd)
                                  <tr>
                                    <td>{{$loop->iteration}}</td>
                                    <td>{{$wd->total_wd}}</td>
                                    <td>
                                      <span class="badge badge-pill {{$wd->status == 'request' ? 'badge-warning' : ($wd->status == 'success' ? 'badge-success' : 'badge-danger')}}">{{$wd->status}}</span>
                                    </td>
                                  </tr>
                                @empty

                                @endforelse
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Modal-->
<div class="modal fade" id="modal-request-wd" data-backdrop="static" tabindex="-1" role="dialog"
    aria-labelledby="staticBackdrop" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-header bg-primary">
                <h5 class="modal-title text-white" id="exampleModalLabel">Modal User</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <i aria-hidden="true" class="ki ki-close"></i>
                </button>
            </div>
            <div class="modal-body">
                <form id="request-wd" method="POST" action="{{ route('statuswd.store') }}" autocomplete="off">
                  @csrf
                  <input type="hidden" name="status" id="status" value="request">
                    <div class="form-group">
                      <label>Total bonus yang dimiliki : {{ $total_bonus }}</label><br>
                      <label for="total_wd">Total Withdraw</label>
                      <input type="text" name="total_wd" class="form-control" id="total_wd" placeholder="Total Withdraw"><br>
                      <label id="message-wd"></label>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-light-primary font-weight-bold" data-dismiss="modal">Tutup</button>
                <button type="button" class="btn btn-primary font-weight-bold" id="saveBtn">Submit</button>
            </div>
        </div>
    </div>
</div>

@push('scripts')
<script>
    $('document').ready(function () {
        $('#requestWithdraw').click(function () {
            $('#saveBtn').val("Request Withdraw");
            $('#user_id').val('');
            $('#request-wd').trigger("reset");
            $('#modal-request-wd').modal('show');                           
        });

        $('#saveBtn').on('click', function(e){
          let totalWd = $('#total_wd').val();
          if (parseInt(totalWd, 10) > {{ $total_bonus }}) {
            alert(`Withdraw melebihi bonus yang dimiliki!`);
            $('#total_wd').focus();
          } else if(totalWd === ''){
            alert(`Withdraw tidak boleh kosong!`);
            $('#total_wd').focus();
          } else if(!parseInt(totalWd, 10)){
            alert('Isi dengan angka!');
            $('#total_wd').focus();
          } else if(parseInt(totalWd, 10) < 0){
            alert('Withdraw tidak boleh kurang dari 0!');
            $('#total_wd').focus();
          }
          else {
            $('#request-wd').submit();
          }
        });

        $('#total_wd').on('keyup', function(e){
          let total_wd = parseInt($(this).val(), 10);
          if (total_wd) {
            let sisa = {{ $total_bonus }} - total_wd;
            if (sisa < 0) {
              $('#message-wd').attr('class', 'text-danger');
            }
            else {
              $('#message-wd').attr('class', 'text-warning');
            }
            $('#message-wd').text('Total bonus yang dimiliki : ' + sisa);
          } else {
            $('#message-wd').text('');
          }
        });
    });
</script>
@endpush